# Deep RL course - FreeCodeCamp

**Disponível em: https://simoninithomas.github.io/deep-rl-course/**

Este repositório abriga os notebooks desenvolvidos durante o curso, os notebooks aqui são cópias feitas por mim durante os meus estudos e são iguais aos desenvolvidos no curso.

## Rodando os projetos

Abra o visual studio code e inicialize os ambientes usando o [poetry](https://python-poetry.org/) na pasta base do projeto, com o comando:

```
    poetry init
```

Entrar no ambiente virtual usando:

```
    poetry shell
```

Então acesse a pasta do agente que gostaria de executar e basta execute as células do projeto usando o ambiente do vscode.
